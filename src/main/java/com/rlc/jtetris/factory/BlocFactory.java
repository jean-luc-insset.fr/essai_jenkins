package com.rlc.jtetris.factory;

import com.rlc.jtetris.bean.Bloc;

/**
 * Modification de JLD le 14/04/2023
 * Interface commune aux fabriques de blocs
 * @author richard
 * @version 1.0.0
 * @since 1.0.0
 */
public interface BlocFactory {

	/**
	 * Cr�� un nouveau bloc
	 * @return Un nouveau bloc
	 */
	Bloc nouveauBloc();
	
}
